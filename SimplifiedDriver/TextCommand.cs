﻿using System;
using SimplifiedDriver.Interfaces;

namespace SimplifiedDriver
{
    public class TextCommand : ICommand
    {
        private string text;

        public TextCommand(string text)
        {
            this.text = text;
        }

        public void Execute()
        {
            Console.WriteLine(text);
        }
    }
}
