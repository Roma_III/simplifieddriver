﻿using System;

namespace SimplifiedDriver
{
    class Program
    {
        static void Main(string[] args)
        {
            string result;
            do
            {
            
            Console.WriteLine("Enter your command !");

            var text = Console.ReadLine();

            Console.WriteLine("Command : " + text);

            var package = new Package(text);

            package.Handle();

             Console.WriteLine("Press any key to continye or 2 to exit ");

            result = Console.ReadLine();

            } while (result != "2");
        }
    }
}
