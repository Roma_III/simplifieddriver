﻿using SimplifiedDriver.Interfaces;
using System;
using System.Linq;

namespace SimplifiedDriver
{
    public class Package
  {
        private ICommand command;
        private char[] commandTypes = new char[]
        {
            'T', 'S'
        };

        public void Handle()
        {
            if (command == null)
            {
                Console.WriteLine("NACK");
            }
            else
            {
                command.Execute();
            }
        }

        public Package(string text)
        {
            var splitedExpression = text.Split(":");

            if (splitedExpression.Length != 3)
            {
                Console.WriteLine("Package is not valid");
                return;
            }

            var prefix = splitedExpression[0];
            if (!prefix.StartsWith("P"))
            {
                Console.WriteLine("Package is not valid");
                return;
            }

            if (prefix.Length != 2)
            {
                Console.WriteLine("Package is not valid");
                return;
            }

            var ending = splitedExpression[2];
            if (ending != "E")
            {
                Console.WriteLine("Package is not valid");
                return;
            }

            var commandType = prefix[1];
            if (!commandTypes.Contains(commandType))
            {
                Console.WriteLine("Package is not valid");
                return;
            }

            if (commandType == 'T')
            {
                var t = splitedExpression[1];

                command = new TextCommand(t);
            }
            else if(commandType == 'S')
            {
                var paramaters = splitedExpression[1].Split(",");

                if (paramaters.Length != 2)
                {
                    Console.WriteLine("Package is not valid");
                    return;
                }

                int.TryParse(paramaters[0], out int freq);
                int.TryParse(paramaters[1], out int duration);

                command = new SoundCommand(freq, duration);
            }
        }
    }
}
