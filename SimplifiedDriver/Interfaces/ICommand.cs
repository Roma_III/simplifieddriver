﻿namespace SimplifiedDriver.Interfaces
{
    public interface ICommand
    {
        void Execute();
    }
}
