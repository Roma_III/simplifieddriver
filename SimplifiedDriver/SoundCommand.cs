﻿using SimplifiedDriver.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;


namespace SimplifiedDriver
{
    public class SoundCommand : ICommand
    {
        private int freq;
        private int duration;

        public SoundCommand(int freq, int duration)
        {
            this.freq = freq;
            this.duration = duration;
        }

        public void Execute()
        {
            Console.Beep(freq, duration);
        }
    }
}
